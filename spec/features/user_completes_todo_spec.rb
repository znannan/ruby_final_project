require "rails_helper"

feature "User competes todo" do
    scenario "successfully" do
        sign_in

        create_todo "Submit final-project"

        click_on "Mark complete"

        expect(page).to display_completed_todo "submit final-project"
    
    end

    

end


