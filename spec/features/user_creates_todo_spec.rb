require "rails_helper"

feature "User creates todo" do
    scenario "successfully" do
        sign_in

        create_todo "Submit final-project"

        #expect(page).to have_css '.todos li', text: "submit final-project"
        expect(page).to display_todo "submit final-project"


    end
end
