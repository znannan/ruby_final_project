require "rails_helper"

feature "User marks todo incompete" do
    scenario "successfully" do
        sign_in

        create_todo "Submit final-project"

        click_on "Mark complete"
        click_on "Mark incomplete"

        expect(page).not_to display_completed_todo "submit final-project"
        expect(page).to display_todo "submit final-project"
    end

    

end
