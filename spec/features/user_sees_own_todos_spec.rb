require "rails_helper"

feature "user sees own todos" do
    scenario "doesn't see others' todos " do
        Todo.create!(title: "Submit final-project", email: "test@test.com")

        sign_in_as "test2@test.com"

        #expect(page).not_to have_css ".todos li", text: "submit final-project"
        expect(page).not_to display_todo "submit final-project"
        
    end
end